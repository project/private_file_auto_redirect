<?php

declare(strict_types=1);

namespace Drupal\private_file_auto_redirect\Controller;

use Drupal\file\FileInterface;
use Drupal\media\MediaInterface;
use Drupal\system\FileDownloadController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Extends the system file controller.
 *
 * Provides auto redirect handling for old files that have been replaced. This
 * is done by redirecting the user to the latest file attached to the entity.
 *
 * @todo make this compatible with any entity type.
 */
class PfarDownloadController extends FileDownloadController {

  /**
   * {@inheritdoc}
   */
  public function download(Request $request, $scheme = 'private') {
    $target = $request->query->get('file');
    // Merge remaining path arguments into relative file path.
    $uri = $scheme . '://' . $target;

    if (!($file = $this->getFileFromUri($uri)) || $file->isTemporary()) {
      return parent::download($request, $scheme);
    }
    if (!$referencing_media = $this->getReferencingMediaFromFile($file)) {
      return parent::download($request, $scheme);
    }
    // Get the latest version of the media.
    $media = $this->entityTypeManager()->getStorage('media')->loadUnchanged($referencing_media->id());
    // If the referencing media is the same as the latest version, bail early
    // for some minor performance wins.
    if ($referencing_media->getRevisionId() === $media->getRevisionId()) {
      return parent::download($request, $scheme);
    }

    $source_field = $media->getSource()->getSourceFieldDefinition($media->get('bundle')->entity);
    // If we can't get the file from the media entity for any reason, fall back.
    if (!$source_field || !$media->hasField($source_field->getName()) || !($latest_file = $media->get($source_field->getName())->entity) || !$latest_file instanceof FileInterface) {
      return parent::download($request, $scheme);
    }

    // If we're looking at the same file as the latest one, fall back.
    if ($latest_file->id() === $file->id()) {
      return parent::download($request, $scheme);
    }

    $target = $this->streamWrapperManager->getTarget($latest_file->getFileUri());
    // Files are different, redirect to the latest file path.
    return $this->redirect('system.private_file_download', [
      'filepath' => $target,
    ]);
  }

  /**
   * Gets a file based on a uri.
   *
   * @param string $uri
   *   The file uri.
   *
   * @return \Drupal\file\FileInterface|null
   *   The file, or null if one couldn't be found.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getFileFromUri(string $uri) {
    // Get the file record based on the URI. If not in the database just return.
    /** @var \Drupal\file\FileInterface[] $files */
    $files = $this->entityTypeManager()
      ->getStorage('file')
      ->loadByProperties(['uri' => $uri]);
    if (count($files)) {
      foreach ($files as $item) {
        // Since some database servers sometimes use a case-insensitive
        // comparison by default, double check that the filename is an
        // exact match.
        if ($item->getFileUri() === $uri) {
          return $item;
        }
      }
    }
    return NULL;
  }

  /**
   * Gets the referencing media entity of the file.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file.
   *
   * @return \Drupal\media\MediaInterface|null
   *   The media entity, or null if there isn't one.
   */
  protected function getReferencingMediaFromFile(FileInterface $file) {
    $media = $this->getReferencingMediaEntity($file);
    if ($media instanceof MediaInterface) {
      return $media;
    }
    return NULL;
  }

  /**
   * Extract the first media entity that references this file.
   *
   * Multiple media entities shouldn't reference the same file, we just pick
   * the first revision we find.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file.
   *
   * @return \Drupal\media\MediaInterface|null
   *   The media entity, or null if there isn't one.
   */
  protected function getReferencingMediaEntity(FileInterface $file) {
    foreach (file_get_file_references($file) as $field_name => $entity_map) {
      foreach ($entity_map as $referencing_entity_type => $referencing_entities) {
        // Only care about references from media entities.
        if ($referencing_entity_type !== 'media') {
          continue;
        }
        // Return the first entity we find, file_get_file_references will only
        // return a single entry if the same entity references the same file
        // in multiple entries. This always seems to be the latest revision
        // that referenced the file.
        return reset($referencing_entities);
      }
    }
    return NULL;
  }

}
