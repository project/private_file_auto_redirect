<?php

declare(strict_types=1);

namespace Drupal\private_file_auto_redirect\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    $routes = [
      'system.private_file_download',
      'system.files',
    ];
    foreach ($routes as $route_name) {
      if ($route = $collection->get($route_name)) {
        $defaults = $route->getDefaults();
        $defaults['_controller'] = '\Drupal\private_file_auto_redirect\Controller\PfarDownloadController::download';
        $route->setDefaults($defaults);
      }
    }
  }

}
