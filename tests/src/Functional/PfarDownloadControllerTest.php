<?php

declare(strict_types=1);

namespace Drupal\Tests\private_file_auto_redirect\Functional;

use Drupal\file\Entity\File;
use Drupal\file\Plugin\Field\FieldType\FileItem;
use Drupal\media\Entity\Media;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;

/**
 * Tests for the PfarDownloadController class.
 *
 * @group private_file_auto_redirect
 */
class PfarDownloadControllerTest extends BrowserTestBase {

  use MediaTypeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'node',
    'media',
    'file',
    'private_file_auto_redirect',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Default testing media type.
   *
   * @var \Drupal\media\MediaTypeInterface
   */
  private $mediaType;

  /**
   * Field definition of default testing media type source field.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface
   */
  private $sourceField;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Create bundle and modify form display.
    $this->mediaType = $this->createMediaType('file', ['id' => 'testing']);
    $this->sourceField = $this->mediaType->getSource()->getSourceFieldDefinition($this->mediaType);
    $fieldStorageDefinition = $this->sourceField->getFieldStorageDefinition();
    $settings = $fieldStorageDefinition->getSettings();
    $settings['uri_scheme'] = 'private';
    $fieldStorageDefinition->setSettings($settings);
    $fieldStorageDefinition->save();
  }

  /**
   * Tests the download controller.
   */
  public function testPrivateFileDownload(): void {
    $media = $this->createMediaEntity();
    $file1 = $media->{$this->sourceField->getName()}->entity;
    $file2 = File::load(FileItem::generateSampleValue($this->sourceField)['target_id']);
    $file3 = File::load(FileItem::generateSampleValue($this->sourceField)['target_id']);
    $latestUrl = $file3->createFileUrl();

    // Create 2 forward revisions with different files.
    $media->field_media_file = $file2;
    $media->setNewRevision();
    $media->save();
    $media->field_media_file = $file3;
    $media->setNewRevision();
    $media->save();

    $this->drupalGet($file1->createFileUrl());
    // Assert we have been redirected to the latest file url.
    $this->assertStringNotContainsString($file1->createFileUrl(), $this->getSession()->getCurrentUrl());
    $this->assertStringContainsString($latestUrl, $this->getSession()->getCurrentUrl());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet($file2->createFileUrl());
    $this->assertStringNotContainsString($file2->createFileUrl(), $this->getSession()->getCurrentUrl());
    $this->assertStringContainsString($latestUrl, $this->getSession()->getCurrentUrl());
    $this->assertSession()->statusCodeEquals(200);

    // When hitting the latest file, we should stay there.
    $this->drupalGet($latestUrl);
    $this->assertStringContainsString($latestUrl, $this->getSession()->getCurrentUrl());
    $this->assertSession()->statusCodeEquals(200);

    // Ensure we're still using all the functionality of the parent class, e.g
    // access control on a file.
    $media->setUnpublished()->save();
    $this->drupalGet($latestUrl);
    $this->assertSession()->statusCodeEquals(403);

    // Reference a new file and overwrite the revision, effectively making the
    // old file de-referenced. This scenario should produce a 403.
    $file4 = File::load(FileItem::generateSampleValue($this->sourceField)['target_id']);
    $media->field_media_file = $file4;
    $media->setNewRevision(FALSE);
    $media->setPublished()->save();
    $this->drupalGet($latestUrl);
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test media entity creation.
   *
   * @return \Drupal\media\Entity\Media
   *   The created media entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createMediaEntity(): Media {
    $value = FileItem::generateSampleValue($this->sourceField);
    $media = Media::create([
      'name' => 'test',
      'bundle' => 'testing',
      $this->sourceField->getName() => $value['target_id'],
      'status' => 1,
    ]);
    $media->save();
    return $media;
  }

}
